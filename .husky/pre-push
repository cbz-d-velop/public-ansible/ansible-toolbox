#!/bin/bash
set -e

# Variables to track global status
global_status="SUCCESS"
type="pre-push"
DIR=$(basename "$(pwd)")

# Function to run a command and report its status
run_and_check() {
    local description="$1"
    local cmd="$2"
    
    echo -e "Action: from script ${type}: ${description}: IN PROGRESS"
    if eval "$cmd"; then
        status="SUCCESS"
    else
        status="FAILED"
        global_status="FAILED"
    fi
    echo -e "Action: from script ${type}: ${description}: ${status}"
}

# Define two arrays: one for descriptions and one for commands
descriptions=(
    "Validate the current branch name"
    "Lint all README inside the project"
    "Lint all YAML files"
    "Lint the Ansible codebase"
    "Add new editions for the previous tests"
)

commands=(
    "npx --no-install validate-branch-name"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest markdownlint './**.md' --disable MD013"
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest yamllint -c ./.yamllint ."
    "MSYS_NO_PATHCONV=1 docker run --rm -v \"\$(pwd):/app\" -w "/app" labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p ."
    "git update-index --again"
)

# Run all commands in the specified order
echo "====================== Git hook: ${type} ======================"
for i in "${!descriptions[@]}"; do
    run_and_check "${descriptions[i]}" "${commands[i]}"
    echo "--------------------------------------------------------"
done

# Report the global status
if [ "$global_status" = "SUCCESS" ]; then
    result=0
else
    result=1
fi

echo "====================== Git hook: ${type} ======================"
echo -e "Report: all actions done for hook ${type}, global status: ${global_status}"
echo "====================== Git hook: ${type} ======================"
exit $result
